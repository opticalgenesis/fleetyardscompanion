package com.lp.fleetyardscompanion.adapters

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.lp.fleetyardscompanion.MainActivity
import com.lp.fleetyardscompanion.R
import com.lp.fleetyardscompanion.fragments.ExpandedShipFragment
import com.lp.fyardlib.BaseShipModel

class ShipListAdapter(private val list: List<BaseShipModel>, private val c: Context,
                      private val f: Fragment) : RecyclerView.Adapter<ShipListAdapter.ShipViewHolder>() {

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShipViewHolder {
        return ShipViewHolder(LayoutInflater.from(c).inflate(R.layout.item_ship_recycler_layout, parent, false))
    }

    override fun onBindViewHolder(holder: ShipViewHolder, position: Int) {
        holder.tv.text = list[position].name
        holder.manu.text = list[position].manufacturer.name
        Glide.with(c).load(Uri.parse(list[position].storeImage)).into(holder.iv)

        holder.cv.setOnClickListener {
            val args = Bundle()
            args.putParcelable("SHIP_KEY", list[position])

            MainActivity.holderPosition = position

            val esf = ExpandedShipFragment()
            esf.arguments = args

            f.fragmentManager
                ?.beginTransaction()
                ?.setReorderingAllowed(true)
                ?.addSharedElement(holder.iv, holder.iv.transitionName)
                ?.replace(R.id.activity_main_fragment_container,
                    esf,
                    ExpandedShipFragment::class.java.simpleName)
                ?.addToBackStack(null)
                ?.commit()
        }
    }

    inner class ShipViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val cv: CardView = itemView.findViewById(R.id.item_ship_recycler_holder)
        val iv: ImageView = itemView.findViewById(R.id.item_ship_recycler_image)
        val tv: TextView = itemView.findViewById(R.id.item_ship_recycler_name)
        val manu: TextView = itemView.findViewById(R.id.item_ship_recycler_manufacturer)
    }
}