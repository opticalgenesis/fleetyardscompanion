package com.lp.fleetyardscompanion

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.lp.fleetyardscompanion.adapters.ShipListAdapter
import com.lp.fleetyardscompanion.dialogs.IndeterminateProgressDialog
import com.lp.fleetyardscompanion.fragments.MainFragment
import com.lp.fyardlib.BaseShipModel
import com.lp.fyardlib.FleetYardsInterface
import com.lp.fyardlib.Utils
import okhttp3.internal.Util
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {

    companion object {
        var holderPosition = 0
    }

    private lateinit var rv: RecyclerView

    private var pageReached: Int = 1

    private val ships = ArrayList<BaseShipModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportFragmentManager.beginTransaction()
            .replace(R.id.activity_main_fragment_container, MainFragment()).commit()
//        pb.show(supportFragmentManager, "pb")
//        api.getAllShips(200, pageReached).enqueue(object : Callback<List<BaseShipModel>> {
//            override fun onResponse(
//                call: Call<List<BaseShipModel>>,
//                response: Response<List<BaseShipModel>>
//            ) {
//                if (response.isSuccessful) {
//                    Log.d("FY","Successful response")
//                    pb.dismiss()
//                    ships.addAll(response.body()!!)
//                    rv.adapter = ShipListAdapter(ships, this@MainActivity)
//                    rv.adapter?.notifyDataSetChanged()
//                } else {
//                    Log.e("FY", "Failed response")
//                    Log.e("FY", response.message())
//                }
//            }
//
//            override fun onFailure(call: Call<List<BaseShipModel>>, t: Throwable) {
//                Log.e("FY", "onFailure: ${t.localizedMessage}")
//                t.printStackTrace()
//            }
//        })

//        rv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
//                super.onScrollStateChanged(recyclerView, newState)
//                if (!recyclerView.canScrollVertically(1)) {
//                    if (pageReached < 2) {
//                        pb.show(supportFragmentManager, "pb")
//                        api.getAllShips(200, ++pageReached)
//                            .enqueue(object : Callback<List<BaseShipModel>> {
//                                override fun onResponse(
//                                    call: Call<List<BaseShipModel>>,
//                                    response: Response<List<BaseShipModel>>
//                                ) {
//                                    if (response.isSuccessful) {
//                                        pb.dismiss()
//                                        ships.addAll(response.body()!!)
//                                        rv.adapter?.notifyDataSetChanged()
//                                    }
//                                }
//
//                                override fun onFailure(call: Call<List<BaseShipModel>>, t: Throwable) {
//                                    t.printStackTrace()
//                                }
//                            })
//                    }
//                }
//            }
//        })
    }
//
//    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
//        menuInflater.inflate(R.menu.menu_main, menu)
//        return true
//    }
//
//    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
//        when (item?.itemId) {
//            R.id.sort_alphabetical_ship_a_z -> {
//                ships.sortBy { it.name }
//                rv.adapter?.notifyDataSetChanged()
//                return true
//            }
//
//            R.id.sort_alphabetical_ship_z_a -> {
//                if (pageReached < 2) {
//                    val r = Utils.getRetrofitInstance()
//                    val api = r.create(FleetYardsInterface::class.java)
//                    api.getAllShips(200, ++pageReached).enqueue(object : Callback<List<BaseShipModel>> {
//                        override fun onResponse(
//                            call: Call<List<BaseShipModel>>,
//                            response: Response<List<BaseShipModel>>
//                        ) {
//                            if (response.isSuccessful) {
//                                ships.addAll(response.body()!!)
//                                ships.sortByDescending { it.name }
//                                rv.adapter?.notifyDataSetChanged()
//                            }
//                        }
//
//                        override fun onFailure(call: Call<List<BaseShipModel>>, t: Throwable) {
//                            t.printStackTrace()
//                        }
//                    })
//                }
//                return true
//            }
//
//            R.id.sort_alphabetical_manu_a_z -> {
//                ships.sortBy { it.manufacturer.name }
//                rv.adapter?.notifyDataSetChanged()
//                return true
//            }
//
//            R.id.sort_alphabetical_manu_z_a -> {
//                if (pageReached < 2) {
//                    val r = Utils.getRetrofitInstance()
//                    val api = r.create(FleetYardsInterface::class.java)
//                    api.getAllShips(200, ++pageReached).enqueue(object : Callback<List<BaseShipModel>> {
//                        override fun onResponse(
//                            call: Call<List<BaseShipModel>>,
//                            response: Response<List<BaseShipModel>>
//                        ) {
//                            if (response.isSuccessful) {
//                                ships.addAll(response.body()!!)
//                                ships.sortByDescending { it.manufacturer.name }
//                                rv.adapter?.notifyDataSetChanged()
//                            }
//                        }
//
//                        override fun onFailure(call: Call<List<BaseShipModel>>, t: Throwable) {
//                            t.printStackTrace()
//                        }
//                    })
//                }
//                return true
//            }
//        }
//        return super.onOptionsItemSelected(item)
//    }
}
