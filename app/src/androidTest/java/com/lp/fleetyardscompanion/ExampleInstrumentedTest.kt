package com.lp.fleetyardscompanion

import android.util.Log
import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.lp.fyardlib.BaseShipModel
import com.lp.fyardlib.FleetYardsInterface
import com.lp.fyardlib.Utils

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.lp.fleetyardscompanion", appContext.packageName)
    }
}

@RunWith(AndroidJUnit4::class)
class FleetTests {
    @Test
    fun fetchShips() {
        val r = Utils.getRetrofitInstance()
        val api = r.create(FleetYardsInterface::class.java)
        api.getAllShips(200).enqueue(object : Callback<List<BaseShipModel>> {
            override fun onResponse(
                call: Call<List<BaseShipModel>>,
                response: Response<List<BaseShipModel>>
            ) {
                if (response.isSuccessful) {
                    assert(response.body()!!.isNotEmpty())
                    assert(response.body()?.get(0)?.name != null)
                }
            }

            override fun onFailure(call: Call<List<BaseShipModel>>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    @Test
    fun fetchSlugs() {
        val r = Utils.getRetrofitInstance()
        val api = r.create(FleetYardsInterface::class.java)
        api.getAllSlugs().enqueue(object : Callback<List<String>> {
            override fun onResponse(call: Call<List<String>>, response: Response<List<String>>) {
                if (response.isSuccessful) {
                    assert(response.body()!!.isNotEmpty())
                }
            }

            override fun onFailure(call: Call<List<String>>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }

    @Test
    fun test600i() {
        val r = Utils.getRetrofitInstance()
        val api = r.create(FleetYardsInterface::class.java)
        api.getShipForSlug("600i-explorer").enqueue(object : Callback<BaseShipModel> {
            override fun onResponse(call: Call<BaseShipModel>, response: Response<BaseShipModel>) {
                assert(response.isSuccessful)
            }

            override fun onFailure(call: Call<BaseShipModel>, t: Throwable) {
                t.printStackTrace()
            }
        })
    }
}