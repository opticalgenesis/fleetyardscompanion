package com.lp.fyardlib

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Video(
    val id: String,
    val url: String,
    val videoId: String,
    val type: String,
    val createdAt: String,
    val updatedAt: String
): Parcelable