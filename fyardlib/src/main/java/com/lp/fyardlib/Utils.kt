package com.lp.fyardlib

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class Utils {
    companion object {
        fun getRetrofitInstance(): Retrofit {
            return Retrofit.Builder()
                .baseUrl("https://api.fleetyards.net/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
        }
    }
}